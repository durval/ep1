#include "urna.hpp"

#include <iostream>

using namespace std;

int main()
{
    /* Objeto principal do projeto */
    Urna urna;

    /* Variável que recebe os erros do método init */
    bool erros;

    /* Método que inicializar o funcionamento da urna */
    erros = urna.init();
    if(erros)
    {
        cout << "Não foi possível abrir o arquivo CSV!" << endl;
        return 1;
    }

    return 0;
}