#include "CSVReader.hpp"

using namespace std;

/* Construtor padrão privado -> não pode ser utilizado fora da classe */
CSVReader::CSVReader()
{
    set_fileName("");
    set_delimiter(';');
}

/* Único construtor público */
CSVReader::CSVReader(string fileName, char delimiter)
{
    set_fileName(fileName);
    set_delimiter(delimiter);
}

CSVReader::~CSVReader() {}

void CSVReader::set_fileName(string fileName)
{
    this->fileName = fileName;
}

void CSVReader::set_delimiter(char delimiter)
{
    this->delimiter = delimiter;
}

string CSVReader::get_fileName()    { return fileName; }

char CSVReader::get_delimiter()     { return delimiter; }

/* Método recebe uma referência para um objeto Urna e carrega todos os candidados do arquivo CSV nos vectors da urna */
bool CSVReader::load_candidatos(Urna * urna)
{
    /* Vector que irá receber todas as linhas do CSV */
    vector<vector<string>> linhas;

    /* booleano para verificar se a função ñ apresentou erros */
    bool erros;

    /* Método para carregar todo o conteúdo do arquivo CSV no vector linhas */
    erros = ler_csv(&linhas);
    if(erros) return 1;

    /* Vector para selecionar os incide das colunas relevantes do arquivo csv */
    vector <int> colunasRelevantes;

    /* Método para preenxer encontrar e salvar os índices das colunas relevante sdo arquivo */
    seleciona_colunas(&colunasRelevantes, &linhas);

    /* Método para criar objetos candidatos com os dados das colunas relevantes do arquivo e salvá-los em um dos vectors da urna */
    load_candidatos_to_urna(&linhas, &colunasRelevantes, urna);

    return 0;
}

/* Método para carregar todo o conteúdo do arquivo CSV no vector linhas */
bool CSVReader::ler_csv( vector<vector<string>> * linhas )
{
    /* Diretório padrão onde estão os arquivos csv */
    string diretorio = "dados/";

    /* Recebo o nome do arquivo CSV */
    string file_name = get_fileName();

    /* Concatenação de strings resulta no caminho completo para o arquivo csv */
    string path = diretorio + file_name;

    /* Conversão de 'string' para 'char*' */
    fstream file(path.c_str());

    /* Caso ocorra algum erro para abrir o arquivo */
    if(!file.is_open()) return 1;

    /* Variável que irá receber as linhas */
    string buffer;

    while(!file.eof())
    {
        /* Recebo tudo até o \n e coloco no buffer */
        getline(file, buffer);

        /* Coloco o buffer em um stream de strings */
        stringstream ss (buffer);

        /* vector de strings para recber os dados separados pelo delimitador do arquivo csv, neste caso o ';' */
        vector<string> linha;

        /* Enquanto não for o final da linha, pego os dados separados pelo delimitador e salvo no buffer */
        while( getline( ss, buffer, get_delimiter() ) )
        {
            /* Remover as aspas excendetes. EX.: ""CARGO"" */
            buffer = buffer.substr(1, buffer.size() - 2);

            /* vector linha recebe o dado coletado */
            linha.push_back(buffer);
        }
        
        /* O vector de vectors recebe a primeira linha */
        (*linhas).push_back(linha);
    }
    
    /* É fechado o arquivo CSV */
    file.close();

    return 0;
}

/* Método para preenxer encontrar e salvar os índices das colunas relevante sdo arquivo */
void CSVReader::seleciona_colunas(vector<int>*colunasRelevantes, vector<vector<string>>*linhas)
{
    /* Para cada coluna da primeira linha do arquivo CSV */
    for (size_t i = 0; i < (*linhas)[0].size(); i++)
    {
        /* string que irá receber o conteudo do vector linhas */
        string nomeCol;

        /* string recebe o conteudo da string */
        nomeCol = (*linhas)[0][i];

        /* Se a string for uma dessas colunas o indice é salvo */
        if (nomeCol == "DS_CARGO" || nomeCol == "NR_CANDIDATO" || nomeCol == "NM_CANDIDATO" || nomeCol == "NM_URNA_CANDIDATO" || nomeCol == "NM_PARTIDO" || nomeCol == "SG_PARTIDO") 
        {
            /* É salvo o índice da coluna achada */
            (*colunasRelevantes).push_back(i);
        }
    }
}

/* Método para criar objetos candidatos com os dados das colunas relevantes do arquivo e salvá-los em um dos vectors da urna */
void CSVReader::load_candidatos_to_urna(vector<vector<string>>*linhas, vector<int>*colunasRelevantes, Urna * urna)
{
    /* Vector de string que irá receber os dados do csv que seram usados para criar cada um dos objetos candidatos */
    vector <string> v_cand;

    /* Loop para cada linha do arquivo csv, pulando a primeira linha (descrição da coluna) */
    for (size_t i = 1; i < (*linhas).size(); i++) 
    {
        /* Removendo os dados de loops anteriores */
        v_cand.clear();

        /* Loop para cada coluna relevante */
        for (size_t j = 0; j < (*colunasRelevantes).size(); j++) 
        {
            /* Recebe o indice de uma coluna relevante */
            int coluna = (*colunasRelevantes)[j];

            /* string recebe uma coluna relevante de um candidato */
            string texto = (*linhas)[i][coluna];

            /* texto adicionado no vector */
            v_cand.push_back(texto);
        }

        /* É utilizado os dados coletados para criar um objeto candidato */
        Candidatos cand(v_cand[0], v_cand[1], v_cand[2], v_cand[3], v_cand[4], v_cand[5]);

        /* Método que adiciona o candidato criado a um vector de acordo com seu cargo */
        (*urna).adc_candidato(cand);
    }
}